from django.db import models
from django.utils import timezone
# Create your models here.

class Ocupacion(models.Model):
    id=models.AutoField(primary_key=True,auto_created=True)
    nombre=models.CharField(max_length=150, null=True,blank=False,verbose_name="Nombre")
    estado=models.BooleanField( default="true")
    fecha_alta = models.DateTimeField(auto_now_add=True, auto_now=False)
    fecha_actualizacion = models.DateTimeField(auto_now_add=False,auto_now=True)
    
    def __str__(self):
        return self.nombre

class Usuario(models.Model):
    id=models.AutoField(primary_key=True,auto_created=True)
    id_ocupacion=models.ForeignKey(Ocupacion,on_delete=models.CASCADE,verbose_name='Id Ocupacion')
    nombre=models.CharField(max_length=150, null=True,blank=False,verbose_name="Nombre")
    apellido=models.CharField(max_length=150,null=False,blank=False,verbose_name="Apellido")
    seg_nombre=models.CharField(max_length=150,null=False,blank=False,verbose_name="Segundo Nombre")
    seg_apellido=models.CharField(max_length=150,null=False,blank=False,verbose_name="Segundo Apellido")
    telefono=models.CharField(max_length=150,null=False,blank=False,verbose_name="Telefono")
    correo=models.CharField(max_length=150, null=True,blank=False,verbose_name="correo")
    contraseña=models.CharField(max_length=150, null=True,blank=False,verbose_name="contraseña")
    foto = models.FileField(max_length=254,upload_to='assets/user/avatar/', default='core/static/core/img/default/defaulavatar.png', verbose_name="Foto")
    foto_Portada = models.FileField(max_length=254,upload_to='assets/user/portada/', default='core/static/core/img/default/defaulPortada.png',verbose_name="Foto Portada")
    estado=models.BooleanField( default="true")
    estado_Web=models.BooleanField( default="true")
    fecha_alta = models.DateTimeField(auto_now_add=True, auto_now=False)
    fecha_actualizacion = models.DateTimeField(auto_now_add=False,auto_now=True)
    
    def __str__(self):
        return self.nombre

#Tipos del Catalogo
class tipos(models.Model):
    id=models.AutoField(primary_key=True,auto_created=True)
    nombre=models.CharField(max_length=150, null=True,blank=True,verbose_name="Nombre")
    foto_Portada = models.FileField(upload_to='assets/guys/avatar/', default='core/static/core/img/default/defaulguys.jpg', verbose_name="Foto")
    titulo=models.CharField(max_length=150, null=True,blank=True,verbose_name="Titulo")
    descripcion=models.CharField(max_length=200,null=False,blank=False,default='?',verbose_name="Descripcion Breve")
    estado=models.BooleanField(default="true")
    fecha_alta = models.DateTimeField(auto_now_add=True, auto_now=False)
    fecha_actualizacion = models.DateTimeField(auto_now=True,auto_now_add=False,)
    
    def __str__(self):
        return self.nombre

#Prototipo 
class Prototipo(models.Model):
  id=models.AutoField(primary_key=True,auto_created=True)
  id_user=models.ForeignKey(Usuario, on_delete=models.CASCADE ,verbose_name="Id User")
  id_tipos=models.ForeignKey(tipos,on_delete=models.CASCADE ,verbose_name="Id Tipo")
  foto = models.FileField(max_length=254,upload_to='assets/prototype/foto/', default='core/static/core/img/default/1.jpg', verbose_name="Foto")
  foto_Portada = models.FileField(max_length=254,upload_to='assets/prototype/portada/', default='core/static/core/img/default/defaulguys.jpg',  verbose_name="Foto Portada")
  nombre=models.CharField(max_length=150, null=True,blank=True,verbose_name="Nombre")
  descripcionbreve=models.CharField(max_length=250,null=True,blank=True,verbose_name="Descripcion Breve")
  estado=models.BooleanField( default="true")
  fecha_alta = models.DateTimeField(auto_now_add=True, auto_now=False)
  fecha_actualizacion = models.DateTimeField(auto_now=True,auto_now_add=False,)
  
  class Meta:
        ordering = ['-fecha_alta']

  def __str__(self):
        return self.nombre


#
class Descripcion(models.Model):
   id=models.AutoField(primary_key=True,auto_created=True)
   id_prototipo=models.ForeignKey(Prototipo,on_delete=models.CASCADE)
   id_Usuario=models.ForeignKey(Usuario,on_delete=models.CASCADE)
   titulo=models.CharField(max_length=150, null=True,blank=True,verbose_name="Titulo")
   foto = models.FileField(max_length=254,upload_to='assets/Detprototype/foto/', default='core/static/core/img/default/1.jpg', verbose_name="Foto")
   descripcion=models.TextField(null=True,blank=True,verbose_name="Descripcion")
   estado=models.BooleanField( default="true")
   fecha_alta = models.DateTimeField(auto_now_add=True, auto_now=False)
   fecha_actualizacion = models.DateTimeField(auto_now=True,auto_now_add=False)

   class Meta:
       ordering = ['-fecha_alta']   
   def __str__(self):
       return self.titulo

#Galeria del prototipo
class Galeria(models.Model):
    id=models.AutoField(null=False,primary_key=True)
    id_prototipo=models.ForeignKey(Prototipo,on_delete=models.CASCADE)
    id_Usuario=models.ForeignKey(Usuario,on_delete=models.CASCADE)
    foto = models.FileField(max_length=254,upload_to='assets/prototype/foto/', default='core/static/core/img/default/2.jpg',verbose_name="Foto")
    descripcionbreve=models.CharField(max_length=200,null=True,blank=True,default='?',verbose_name="Descripcion Breve")
    estado=models.BooleanField(default="true")
    fecha_alta = models.DateTimeField(auto_now_add=True, auto_now=False)
    fecha_actualizacion = models.DateTimeField(auto_now=True,auto_now_add=False,)

    def __str__(self):
        return self.id_prototype.nombre

#Comentarios del prototipo
class Comentario(models.Model):
    id=models.AutoField(null=False,primary_key=True)
    id_prototipo=models.ForeignKey(Prototipo,on_delete=models.CASCADE)
    id_Usuario=models.ForeignKey(Usuario,on_delete=models.CASCADE)
    comentario=models.CharField(max_length=254, null=True, blank=True, verbose_name="Comentario")
    estado=models.BooleanField( default="true")
    fecha_alta = models.DateTimeField(auto_now_add=True, auto_now=False)
    fecha_actualizacion = models.DateTimeField(auto_now=True,auto_now_add=False,)
    def __str__(self):
        return self.id_prototype.nombre
#Me Gusta del prototipo
class Megusta(models.Model):
    id=models.AutoField(null=False,primary_key=True)
    id_prototipo=models.ForeignKey(Prototipo,on_delete=models.CASCADE)
    id_Usuario=models.ForeignKey(Usuario,on_delete=models.CASCADE)
    estado=models.BooleanField( default="true")
    fecha_alta = models.DateTimeField(auto_now_add=True, auto_now=False)
    fecha_actualizacion = models.DateTimeField(auto_now=True,auto_now_add=False,)
    def __str__(self):
        return self.id_prototype.nombre
#Vista del prototipo  
class Vista(models.Model):
    id=models.AutoField(null=False,primary_key=True)
    id_prototipo=models.ForeignKey(Prototipo,on_delete=models.CASCADE)
    id_Usuario=models.ForeignKey(Usuario,on_delete=models.CASCADE)
    estado=models.BooleanField( default="true")
    fecha_alta = models.DateTimeField(auto_now_add=True, auto_now=False)
    fecha_actualizacion = models.DateTimeField(auto_now=True,auto_now_add=False)
    def __str__(self):
        return self.id_prototype.nombre


class contactos(models.Model):
    id=models.AutoField(null=False,primary_key=True)
    nombre=models.CharField(max_length=150,null=True, verbose_name="Nombre")
    apellido=models.CharField(max_length=150,null=True, verbose_name="Apellido")
    ocupacion=models.CharField(max_length=150,null=True, verbose_name="Ocupacion")
    frase=models.CharField(max_length=250,null=True, verbose_name="Frase")
    estrella=models.CharField(max_length=50,null=True, verbose_name="Estrellas")
    foto = models.FileField(upload_to='assets/contacto/foto/',default='core/static/core/img/default/defaulavatar.png', null=True, blank=False, verbose_name="Foto")
    estado=models.BooleanField( default="true")
    fecha_alta = models.DateTimeField(auto_now_add=True, auto_now=False)
    fecha_actualizacion = models.DateTimeField(auto_now=True,auto_now_add=False)

    class Meta:
        ordering = ['estrella']

    def __str__(self):
        return self.nombre