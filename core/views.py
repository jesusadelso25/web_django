from django.shortcuts import render,redirect,get_list_or_404,HttpResponse
from django.contrib.auth import authenticate,login
from django.contrib import messages
from .models import *


#500: error en el servidor
def pag_500_error_server(request):
    return render(request,'505.html')
#404: página no encontrada
def pag_404_not_found(request,):
    return render(request,'404.html')

#: Mantenimiento de la pagina
def pag_mant(request):
   return render(request,'maintenance.html')
   


def Web_Inicio(request):
    Prototipos=Prototipo.objects.all()[:3]
    views=Vista.objects.all()
    Likes=Megusta.objects.all()
    Comments=Comentario.objects.all()
    ListGeneral=[]
    for prototipo in Prototipos:#Arreglo para mostrar las cantida de views,comment y likes
        CountView=0
        CountComment=0
        CountLike=0
        for vista in views:
            if  vista.id_prototipo.id == prototipo.id:
                CountView=CountView+1
        for Gusta in Likes:
            if  Gusta.id_prototipo.id == prototipo.id:
                CountLike=CountLike+1
        for Coment in Comments:
            if  Coment.id_prototipo.id == prototipo.id:
                CountComment=CountComment+1
        ListGeneral.append([prototipo.id,prototipo.foto,prototipo.nombre,prototipo.descripcionbreve,CountView,CountLike,CountComment])
    return render(request,'core/web/views/inicio.html',context={'ListGeneral':ListGeneral})
def Web_Nosotros(request):
    contact=contactos.objects.all()
    id=0 #Id del ultimo contacto 
    for cont in contact:
       id=cont.id
    return render(request,'core/web/views/nosotros.html',context={'contact':contact,'id':id})
def Web_Prototipos(request):
    Prototipos=Prototipo.objects.all()
    views=Vista.objects.all()
    Likes=Megusta.objects.all()
    Comments=Comentario.objects.all()
    Tipo=tipos.objects.all()
    TipCount=0
    ListGenerale=[]
    for Tip in Tipo:#Determinar el id del ultima foto de portada del Tipo de prototipo
        TipCount=Tip.id
    for prototipo in Prototipos:#Arreglo para mostrar las cantida de views,comment y likes
        CountView=0
        CountComment=0
        CountLike=0
        for vista in views:
            if  vista.id_prototipo.id == prototipo.id:
                CountView=CountView+1
        for Gusta in Likes:
            if  Gusta.id_prototipo.id == prototipo.id:
                CountLike=CountLike+1
        for Coment in Comments:
            if  Coment.id_prototipo.id == prototipo.id:
                CountComment=CountComment+1
        ListGenerale.append([prototipo.id,prototipo.foto,prototipo.nombre,prototipo.descripcionbreve,CountView,CountLike,CountComment,prototipo.id_tipos.id])
    return render(request,'core/web/views/prototipos.html',context={'ListGeneral':ListGenerale,'Tipos':Tipo,'TipCount':TipCount})
def Web_Detall_Prototipos(request,id):
    try:
        Prototipos=Prototipo.objects.get(id=id)
        Desc=Descripcion.objects.all()
        Gareria=Galeria.objects.all().filter(estado='True')
        views=Vista.objects.all().filter(estado='True')
        Likes=Megusta.objects.all().filter(estado='True')
        Comments=Comentario.objects.all().filter(estado='True')
        ListGareria=[]
        CountView=0
        CountComment=0
        CountLike=0
        for Gareria in Gareria:
            if  Gareria.id_prototipo.id == Prototipos.id:
                ListGareria.append([Gareria.id,Gareria.foto,Gareria.descripcionbreve])
        for vista in views:
            if  vista.id_prototipo.id == Prototipos.id:
                CountView=CountView+1
        for Gusta in Likes:
            if  Gusta.id_prototipo.id == Prototipos.id:
                CountLike=CountLike+1
        for Coment in Comments:
            if  Coment.id_prototipo.id == Prototipos.id:
                CountComment=CountComment+1
        return render(request,'core/web/views/detalleprototipo.html',context={'Prototipos':Prototipos,'ListGareria':ListGareria,'CountView':CountView,'CountComment':CountComment,'CountLike':CountLike,'Desc':Desc  })
    except Exception as ex :
        return render(request,'404.html')

def Web_Novedades(request):
    Prototipos=Prototipo.objects.all()[:12]
    views=Vista.objects.all()
    Likes=Megusta.objects.all()
    Comments=Comentario.objects.all()
    Tipo=tipos.objects.all()
    TipCount=0
    ListGeneral=[]
    for Tip in Tipo:#Determinar el id del ultima foto de portada del Tipo de prototipo
        TipCount=Tip.id
    for prototipo in Prototipos:#Arreglo para mostrar las cantida de views,comment y likes
        CountView=0
        CountComment=0
        CountLike=0
        for vista in views:
            if  vista.id_prototipo.id == prototipo.id:
                CountView=CountView+1
        for Gusta in Likes:
            if  Gusta.id_prototipo.id == prototipo.id:
                CountLike=CountLike+1
        for Coment in Comments:
            if  Coment.id_prototipo.id == prototipo.id:
                CountComment=CountComment+1
        ListGeneral.append([prototipo.id,prototipo.foto,prototipo.nombre,prototipo.descripcionbreve,CountView,CountLike,CountComment])
    return render(request,'core/web/views/novedades.html',context={'ListGeneral':ListGeneral,'Tipos':Tipo,'TipCount':TipCount})
def Web_Contactos(request):
    contact=contactos.objects.all()
    id=0 #Id del ultimo contacto 
    for cont in contact:
       id=cont.id
    return render(request,'core/web/views/contactos.html',context={'contact':contact,'id':id})
def Web_Login(request): 
    estado=True
    if request.method== 'POST':
        return HttpResponse(request.POST)
        #usuario=request.POST['email']
        #if usuario =="":
        #    estado=False
        #    messages.warning(request, 'Debes insertar un usuario')
        #if estado:
        #    estado=False
        #    contraseña=request.POST['password']
        #    if contraseña =="":
        #        messages.warning(request, 'Debes insertar una contraseña')
        #if estado:
        #    Usuario=authenticate(request,correo=usuario,contraseña=contraseña)
        #    if Usuario is not None:
        #        login(request, Usuario)
        #        messages.success(request, 'Este es un mensage de prueba de correcto') 
        #        return redirect('/INICIO')
        #    else:
        #        messages.error(request, 'Este es un mensage de prueba de Error')   
    return render(request,'core/web/views/login.html')
def Web_Regist(request):
    estado=True
    if request.method== 'POST':
        return HttpResponse(request.POST)
    return render(request,'core/web/views/regist.html')
def Web_Perfil(request):
    return render(request,'core/web/views/perfil.html')

# Vista Admin
def Login(request):
    return render(request,'core/admin/views/login.html')
def PanelPrincipal(request):
    return render(request,'core/admin/views/panelprincipal.html')





