from django.urls import path
from . import views


app_name='core'

urlpatterns = [
    path('404/',views.pag_404_not_found),
    path('505/',views.pag_500_error_server),
    path('MANT/',views.pag_mant),

    # ruta personalizada para manejar en /admin/
    path('',views.Web_Inicio, name='INICIO'),
    path('INICIO/',views.Web_Inicio, name='Nosotros'),
    path('NOSOTROS/',views.Web_Nosotros, name='Nosotros'),
    path('PROTOTIPOS/',views.Web_Prototipos, name='Prototipos'),
    path('NOVEDADES/',views.Web_Novedades, name='Novedades'),
    path('PROTOTIPO/DETALLE/<int:id>/',views.Web_Detall_Prototipos, name='Detalle Prototipo'),
    path('CONTACTOS/',views.Web_Contactos, name='Contactos'),
    path('LOGIN/',views.Web_Login, name='Login'),
    path('REGIST/',views.Web_Regist, name='Registral'),
    path('PERFIL/1/',views.Web_Perfil, name='Registral'),
    
    # ruta personalizada para manejar en /admin/
    path('ADMIN/',views.PanelPrincipal, name='Panel Principal'),
    path('ADMIN/LOGIN/',views.Login, name='Login'),
    path('ADMIN/PANELPRINCIPAL/',views.PanelPrincipal, name='Panel Principal'),  
]



